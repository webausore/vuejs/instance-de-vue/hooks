# vuejs-instance-de-vue-hooks

Preview : https://vuejs-instance-de-vue-hooks.netlify.app/

Exercice :

-   Créer une structure vue avec index.html / script.js
-   Créer l'instance de vue en initialisant un objet data avec une valeur :
    -   valeur = 'je like vuejs'
    -   Brancher tous les hooks présent sur le diagramme du cours, à l'instance de vue précédemment créée
    -   La fonction associé à chaque hook devra simplement console.log l'évènement. Par ex : beforeCreate -> console.log('beforeCreate')
    -   Créer des comportements dans l'application Vue pour déclencher chaque hook de cycle de vie.

Ressource :

-   Documentation :
    -   Hook : https://fr.vuejs.org/v2/guide/instance.html#Hooks-de-cycle-de-vie-d%E2%80%99une-instance
    -   Diagramme cycle de vie : https://fr.vuejs.org/v2/guide/instance.html#Diagramme-du-cycle-de-vie
-   Vidéo : https://vueschool.io/lessons/understanding-the-vuejs-lifecycle-hooks?friend=vuejs (regarde bien cette vidéo si tu veux avoir la liste des hooks à brancher et comment relier un callback)
