let vm = new Vue({
  el: "#app",

  data: {
    valeur: "je like vuejs",
  },

  methods: {
    destroy: function () {
      this.$destroy();
    },
  },

  beforeCreate: function () {
    console.log("beforeCreate");
  },

  created: function () {
    console.log("created");
  },

  beforeMount: function () {
    console.log("beforeMount");
  },

  mounted: function () {
    console.log("mounted");
  },

  beforeDestroy: function () {
    console.log("beforeDestroy");
  },

  destroyed: function () {
    console.log("destroyed");
  },
});
